const express = require('express');
const app = express(); 
const server = app.listen(1337);
app.use(express.static(__dirname));
const io = require('socket.io')(server); 
console.log('listen on port 1337')

// client count
let clientCount = 0
// use to save client socket
let socketMap = {}

// 

const socketListener = function(socket, action) {
    socket.on(action, data=>{
        if(socket.clientNum % 2 == 0){
            if(socketMap[socket.clientNum - 1]) socketMap[socket.clientNum - 1].emit(action, data)
        }else{
            if(socketMap[socket.clientNum + 1]) socketMap[socket.clientNum + 1].emit(action, data)
        }
    })
}

//
io.on('connection', function(socket) {

    clientCount = clientCount + 1
    socket.clientNum = clientCount
    socketMap[clientCount] = socket
    console.log(socket.clientNum)
    // before game start
    if(clientCount % 2 == 1){
        socket.emit('waiting', {msg: 'waiting for another player'})
    }else{
        if(socketMap[(clientCount - 1)]){
            socket.emit('start')
            socketMap[(clientCount - 1)].emit('start')
        }else{
            socket.emit('leave')
        }
    }

    // received init data
    socketListener(socket, 'init')
    // perform next block
    socketListener(socket, 'next')
    // up
    socketListener(socket, 'up')
    // right
    socketListener(socket, 'right')
    // down
    socketListener(socket, 'down')
    // left
    socketListener(socket, 'left')
    // space
    socketListener(socket, 'fall')
    // make block stay
    socketListener(socket, 'fixed')
    // clear lines
    socketListener(socket, 'line')
    // time passed
    socketListener(socket, 'time')
    // opponent lose
    socketListener(socket, 'lose')
    // add Lines
    socketListener(socket, 'bottomLine')
    // update to remote
    socketListener(socket, 'addTailLines')
    //
    socket.on('disconnect', function(){
        console.log('discon')
        if(socket.clientNum % 2 == 0){
            if(socketMap[socket.clientNum - 1]) socketMap[socket.clientNum - 1].emit('leave')
        }else{
            if(socketMap[socket.clientNum + 1]) socketMap[socket.clientNum + 1].emit('leave')
        }
        delete(socketMap[socket.clientNum])
    })

})