class Square{
    constructor(){
        // block data
        this.data = [
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
        ]
        // position
        this.origin = {
            x: 0,
            y: 0
        }
        // direction
        this.dir = 0
    }
    // test it if block can move
    blockTest(check, direction) {
        let test = {}
        switch(direction){
            case 'down':
                test.x = this.origin.x + 1
                test.y = this.origin.y
            break
            case 'left':
                test.x = this.origin.x
                test.y = this.origin.y - 1
            break
            case 'right':
                test.x = this.origin.x 
                test.y = this.origin.y + 1
            break
            case 'up':
                let d = (this.dir + 1) % 4
                let uptest = [
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                    [0, 0, 0, 0],
                ]
                for(let i in this.data){
                    for(let j in this.data[0]){
                        uptest[i][j] = this.rotates[d][i][j]
                    }
                }
                return check.checkData(this.origin, uptest)
            break
        }
        return check.checkData(test, this.data)
    }
    // block set move position
    blockMove(direction, num) {
        switch(direction){
            case 'down':
                this.origin.x += 1
            break
            case 'left':
                this.origin.y -= 1
            break
            case 'right':
                this.origin.y += 1
            break
            case 'up':
                if(!num) num = 1
                this.dir = (this.dir + num) % 4
                for(let i in this.data){
                    for(let j in this.data[0]){
                        this.data[i][j] = this.rotates[this.dir][i][j]
                    }
                }
            break
        }
    }
    //
}