class Remote {
    constructor(socket) {
        // game object
        this.game = new Game();
        //
        this.socket = socket
        this.socket_init()
    }
   
    //
    start(type, dir) {
        // get element obj on html
        let doms = {
            gameDiv: document.getElementById('remote_game'),
            nextDiv: document.getElementById('remote_next'),
            timeDiv: document.getElementById('remote_time'),
            scoreDiv: document.getElementById('remote_score'),
            resultDiv: document.getElementById('remote_result'),
            upButton: document.getElementById('remote_up'),
            leftButton: document.getElementById('remote_left'),
            downButton: document.getElementById('remote_down'),
            rightButton: document.getElementById('remote_right'),
            spaceButton: document.getElementById('remote_space'),
        }
        // init all data
        this.game.init(doms, type, dir )
    }
    
    //  -----------   socket - all from your opponent
    socket_init() {

        this.socket.on('init', data=>{
            this.start(data.type, data.dir)
        })

        this.socket.on('next', data=>{
            this.game.createNext(data.type, data.dir)
        })

        this.socket.on('up', data=>{
            this.game.keyMove('up')
            this.game.KeyStyleChange('up')
        })

        this.socket.on('left', data=>{
            this.game.keyMove('left')
            this.game.KeyStyleChange('left')
        })

        this.socket.on('down', data=>{
            this.game.keyMove('down')
            this.game.KeyStyleChange('down')
        })

        this.socket.on('right', data=>{
            this.game.keyMove('right')
            this.game.KeyStyleChange('right')
        })

        this.socket.on('fall', data=>{
            this.game.fall()
            this.game.KeyStyleChange('space')
        })

        this.socket.on('fixed', data=>{
            this.game.stay()
        })

        this.socket.on('line', data=>{
            this.game.checkFinish()
            this.game.addScore(data)
        })

        this.socket.on('time', data=>{
            this.game.setTime(data)
        })

        this.socket.on('lose', data=>{
            this.game.gameover(false)
        })

        this.socket.on('addTailLines', data=>{
            this.game.addLineBottom(data)
        })
    }
    //
}