class Local {
    constructor(socket) {
        // game object
        this.game = new Game();
        this.INTERVAL = 300
        // time
        this.timeCount = 0
        this.time = 0
        //
        this.socket = socket
        this.socket_receive()
    }
    // bind key
    bindKeyEvent() {
        document.onkeydown = e =>{
            switch(e.keyCode){
                case 38: // up
                    this.game.keyMove('up')
                    this.game.KeyStyleChange('up')
                    this.socket_send('up') // send to my opponent
                break
                case 39: // right
                    this.game.keyMove('right')
                    this.game.KeyStyleChange('right')
                    this.socket_send('right') // send to my opponent
                break
                case 40: // down
                    this.game.keyMove('down')
                    this.game.KeyStyleChange('down')
                    this.socket_send('down') // send to my opponent
                break
                case 37: // left
                    this.game.keyMove('left')
                    this.game.KeyStyleChange('left')
                    this.socket_send('left') // send to my opponent
                break
                case 32: // space
                    this.game.fall()
                    this.game.KeyStyleChange('space')
                    this.socket_send('fall') // send to my opponent
                break
            }
        }
    }
    //
    start() {
        // get element obj on html
        let doms = {
            gameDiv: document.getElementById('local_game'),
            nextDiv: document.getElementById('local_next'),
            timeDiv: document.getElementById('local_time'),
            scoreDiv: document.getElementById('local_score'),
            resultDiv: document.getElementById('local_result'),
            upButton: document.getElementById('local_up'),
            leftButton: document.getElementById('local_left'),
            downButton: document.getElementById('local_down'),
            rightButton: document.getElementById('local_right'),
            spaceButton: document.getElementById('local_space'),
        }

        ////////////////////   init all data
        let type = this.randomType()
        let dir = this.randomDir()
        this.game.init(doms, type, dir)
        this.socket_send('init', {type:type, dir:dir}) // send to my opponent
        //////////////////////////
        
        this.bindKeyEvent() // bind keypress to game

        //////////////////// prepare for next block
        let t = this.randomType()
        let y = this.randomDir()
        this.game.createNext(t, y) 
        this.socket_send('next', {type:t, dir:y}) // send to my opponent
        ////////////////////


        // timer for block go down slowly
        this.timer = setInterval(() => {
            this.move()
        }, this.INTERVAL);
    }
    // block go down slowly
    move() {
        this.timeProcess()
        if(!this.game.keyMove('down')){
            ///////
            this.game.stay() // change it style
            this.socket_send('fixed') // send to my opponent
            ///////

            /////////
            let line = this.game.checkFinish() // check if any lines that done
            if(line) {
                this.game.addScore(line) // add score if clear any line
                this.socket_send('line', line) // send to my opponent
                if(line >= 2){
                    let bottomLine = this.generateBottomLine(line-1)
                    this.socket_send('bottomLine', bottomLine)
                }
            } 
            ///////

            let gameOver = this.game.checkGameOver() //
            if(gameOver){
                this.game.gameover(false)
                this.stop()
                this.socket_send('lose')
            }else{
                ///////
                let t = this.randomType()
                let d = this.randomDir()
                this.game.createNext(t, d)  // prepare for next block
                this.socket_send('next', {type:t, dir:d}) // send to my opponent
                ///////
            }
        }else{
            this.socket_send('down') // send to my opponent
        }
    }
    // time
    timeProcess() {
        this.timeCount += 1
        if(this.timeCount == 5){
            this.timeCount = 0
            this.time += 1
            this.game.setTime(this.time)
            this.socket_send('time', this.time)
        }
    }
    // random generate interference
    generateBottomLine(linenum){
        let lines = []
        for(let i=0; i<linenum; i++) {
            let line = []
            for(let j=0; j<10; j++){
                line.push(Math.ceil(Math.random() * 2) - 1)
            }
            lines.push(line)
        }
        return lines
    }
    // get random block type
    randomType() {
        return Math.ceil(Math.random() * 7)
    }
    // get random block direction
    randomDir() {
        return Math.ceil(Math.random() * 4)
    }
    // gameover 
    stop() {
        if(this.timer) {
            clearInterval(this.timer)
            this.timer = null
        }
        document.onkeydown = null
    }

    //  -----------   socket
    socket_send(action, data) {
        this.socket.emit(action, data)
    }
    
    socket_receive() {
        //
        this.socket.on('start', ()=>{
            document.getElementById('waiting').innerHTML = `Game Starting now !`
            this.start()
        })
        //
        this.socket.on('lose', ()=>{
            this.stop()
            this.game.gameover(true)
        })
        //
        this.socket.on('leave', ()=>{
            let local = document.getElementById('local_result')
            local.innerHTML = 'You Win ..'
            local.className += ' win'
            document.getElementById('remote_result').innerHTML = 'Disconnect ..'
            this.stop()
        })
        //
        this.socket.on('bottomLine', (data)=>{
            this.game.addLineBottom(data)
            this.socket_send('addTailLines', data)
        })
    }
}
