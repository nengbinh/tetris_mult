window.onload = function(){   

    // init socket io
    const socket = io(); 

    const local = new Local(socket)
    const remote = new Remote(socket)

    socket.on('waiting', data=>{
        document.getElementById('waiting').innerHTML = data.msg
    })
}